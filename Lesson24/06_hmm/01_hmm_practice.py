# %%
import numpy as np
from hmmlearn import hmm
#http://hmmlearn.readthedocs.io/en/latest/

# %% [markdown]
# 隐藏状态：3个盒子

# %%
states = ["box 1", "box 2", "box3"]
n_states = len(states)

# %% [markdown]
# 观测状态：2种球

# %%
observations = ["red", "white"]
n_observations = len(observations)

# %% [markdown]
# 模型参数

# %%
# 初始状态pi
start_probability = np.array([0.2, 0.4, 0.4])

# 状态转移概率矩阵A
transition_probability = np.array([
  [0.5, 0.2, 0.3],
  [0.3, 0.5, 0.2],
  [0.2, 0.3, 0.5]
])

# 状态生成观测概率矩阵B
emission_probability = np.array([
  [0.5, 0.5],
  [0.4, 0.6],
  [0.7, 0.3]
])

# %%
#用于离散观测状态
model = hmm.MultinomialHMM(n_components=n_states)

model.startprob_=start_probability
model.transmat_=transition_probability
model.emissionprob_=emission_probability

# %% [markdown]
# ### 维特比算法，看到的是[0,1,0]

# %%
seen = np.array([[0,1,0]]).T

# %%
logprob, box = model.decode(seen, algorithm="viterbi")

# %% [markdown]
# 得到的结果跟我们算的是一样的

# %%
# box是隐藏状态序列
print(np.array(states)[box])

# %% [markdown]
# predict也可以

# %%
# predict和decode得到的结果是一样的
box2 = model.predict(seen)
print(np.array(states)[box2])

# %% [markdown]
# 得到观测序列的概率 ln0.13022≈−2.0385

# %%
print(model.score(seen))

# %% [markdown]
# ### EM算法求解模型参数

# %% [markdown]
# tol: 停止阈值，没达到最大迭代次数可以提前停止
# n_iter: 最大迭代次数 
# n_components: 隐藏状态数目 

# %%
model2 = hmm.MultinomialHMM(n_components=n_states, n_iter=20, tol=0.01)
X2 = np.array([[0,1,0,1],[0,0,0,1],[1,0,1,1]])
model2.fit(X2)

print('startprob_',model2.startprob_)
print('-----------------------------------------')
print('transmat_',model2.transmat_)
print('-----------------------------------------')
print('emissionprob_',model2.emissionprob_)
print('-----------------------------------------')
print('score',model2.score(X2))
