# %% [markdown]
# ### Mnist分类任务：
# 
# - 网络基本构建与训练方法，常用函数解析
# 
# - torch.nn.functional模块
# 
# - nn.Module模块
# 

# %% [markdown]
# ### 读取Mnist数据集
# - 会自动进行下载

# %%
from pathlib import Path
import requests

DATA_PATH = Path("data")
PATH = DATA_PATH / "mnist"

PATH.mkdir(parents=True, exist_ok=True)

URL = "http://deeplearning.net/data/mnist/"
FILENAME = "mnist.pkl.gz"

if not (PATH / FILENAME).exists():
        content = requests.get(URL + FILENAME).content
        (PATH / FILENAME).open("wb").write(content)

# %%
import pickle
import gzip

with gzip.open((PATH / FILENAME).as_posix(), "rb") as f:
        ((x_train, y_train), (x_valid, y_valid), _) = pickle.load(f, encoding="latin-1")

# %% [markdown]
# 784是mnist数据集每个样本的像素点个数

# %%
from matplotlib import pyplot
import numpy as np

pyplot.imshow(x_train[0].reshape((28, 28)), cmap="gray")
print(x_train.shape)
# (50000, 784)  784=28x28

# %% [markdown]
# 注意数据需转换成tensor才能参与后续建模训练

# %%
import torch

# 将数据转换成tensor格式
x_train, y_train, x_valid, y_valid = map(
    torch.tensor, (x_train, y_train, x_valid, y_valid)
)
n, c = x_train.shape
x_train, x_train.shape, y_train.min(), y_train.max()
print(x_train, y_train)
print(x_train.shape)
print(y_train.min(), y_train.max())

# %% [markdown]
# ### torch.nn.functional 很多层和函数在这里都会见到
# 
# torch.nn.functional中有很多功能，后续会常用的。那什么时候使用nn.Module，什么时候使用nn.functional呢？一般情况下，如果模型有可学习的参数，最好用nn.Module，其他情况nn.functional相对更简单一些，nn.functional在实际项目中用的较少。

# %%
import torch.nn.functional as F

# 交叉熵
loss_func = F.cross_entropy

def model(xb):
    # xw + b
    return xb.mm(weights) + bias

# %%
# batch size
bs = 64
# x的形状(64, 784)
xb = x_train[0:bs]  # a mini-batch from x
yb = y_train[0:bs]
weights = torch.randn([784, 10], dtype = torch.float,  requires_grad = True) 
bs = 64
bias = torch.zeros(10, requires_grad=True)

# loss_func(预测值, 标签)
print(loss_func(model(xb), yb))

# %% [markdown]
# ### 创建一个model来更简化代码
# 
# - 必须继承nn.Module且在其构造函数中需调用nn.Module的构造函数
# - 无需写反向传播函数，nn.Module能够利用autograd自动实现反向传播
# - Module中的可学习参数可以通过named_parameters()或者parameters()返回迭代器

# %%
from torch import nn

class Mnist_NN(nn.Module):
    def __init__(self):
        super().__init__()
        self.hidden1 = nn.Linear(784, 128)
        self.hidden2 = nn.Linear(128, 256)
        # self.hidden3 = nn.Linear(256, 512)
        self.out  = nn.Linear(256, 10)
        self.dropout = nn.Dropout(0.5)

    # 前向传播：x(64, 784)
    def forward(self, x):
        x = F.relu(self.hidden1(x))
        x = self.dropout(x)
        x = F.relu(self.hidden2(x))
        x = self.dropout(x)
        # x = F.relu(self.hidden3(x))
        x = self.out(x)
        return x
        

# %%
net = Mnist_NN()
print(net)

# %% [markdown]
# 可以打印我们定义好名字里的权重和偏置项

# %%
for name, parameter in net.named_parameters():
    print(name, parameter, parameter.size())

# %% [markdown]
# ### 使用TensorDataset和DataLoader来简化

# %%
from torch.utils.data import TensorDataset
from torch.utils.data import DataLoader

train_ds = TensorDataset(x_train, y_train)
train_dl = DataLoader(train_ds, batch_size=bs, shuffle=True)

valid_ds = TensorDataset(x_valid, y_valid)
# 可以不用乘以2
valid_dl = DataLoader(valid_ds, batch_size=bs * 2)

# %%
def get_data(train_ds, valid_ds, bs):
    return (
        DataLoader(train_ds, batch_size=bs, shuffle=True),
        DataLoader(valid_ds, batch_size=bs * 2),
    )

# %% [markdown]
# - 一般在训练模型时加上model.train()，这样会正常使用Batch Normalization和Dropout
# - 测试的时候一般选择model.eval()，这样就不会使用Batch Normalization和Dropout

# %% [markdown]
# ### zip的用法
a = [1, 2, 3]
b = [4, 5, 6]
zipped = zip(a, b)
print(list(zipped))  # [(1, 4), (2, 5), (3, 6)]
a2, b2 = zip(*zip(a, b))
print(a2)  # (1, 2, 3)
print(b2)  # (4, 5, 6)

# %%
import numpy as np

# epoch和batch的区别：假设数据集为10000，batch为每次训练100个数据，1个epoch就是训练一次数据集，相当于迭代训练100个batch。

# steps: 迭代次数，相当于epoch
# model: 模型对象
# loss_func: 损失函数
# opt: 优化器
# train_dl: 训练集DataLoader
# valid_dl: 验证集DataLoader
def fit(steps, model, loss_func, opt, train_dl, valid_dl):
    for step in range(steps):
        # 训练模式：更新w,b
        model.train()
        for xb, yb in train_dl:
            loss_batch(model, loss_func, xb, yb, opt)

        # 验证模式：不更新w,b
        model.eval()
        with torch.no_grad():
            losses, nums = zip(
                *[loss_batch(model, loss_func, xb, yb) for xb, yb in valid_dl]
            )
        # 平均损失
        val_loss = np.sum(np.multiply(losses, nums)) / np.sum(nums)
        print('当前step:'+str(step), '验证集损失：'+str(val_loss))

# %%
from torch import optim
def get_model():
    model = Mnist_NN()
    # return model, optim.Adam(model.parameters(), lr=0.001)
    return model, optim.SGD(model.parameters(), lr=0.001)

# %%
def loss_batch(model, loss_func, xb, yb, opt=None):
    # 计算损失
    loss = loss_func(model(xb), yb)

    if opt is not None:
        # 反向传播
        loss.backward()
        # 更新权重参数w,b
        opt.step()
        # 清空之前的梯度
        opt.zero_grad()

    return loss.item(), len(xb)

# %% [markdown]
# ### 三行搞定！

# %%
train_dl, valid_dl = get_data(train_ds, valid_ds, bs)
model, opt = get_model()
fit(25, model, loss_func, opt, train_dl, valid_dl)

# %%
# 计算当前模型的准确率
correct = 0
total = 0
for xb, yb in valid_dl:
    outputs = model(xb)
    # 返回每一行最大的值和索引
    _, predicted = torch.max(outputs.data, dim=1)
    total += yb.size(0)
    correct += (predicted == yb).sum().item()

print('Accuracy of the network on the 10000 test images: %d %%' % (100 * correct / total))
