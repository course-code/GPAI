## THUCNews

- data
  - train.txt 训练集
  - dev.txt 验证集
  - test.txt 测试集
  - class.txt 标签
  - vocab.pkl 语料表
  - embedding_SougouNews.npz 搜狗embedding
  - embedding_Tencent.npz 腾讯embedding
- log 日志
- saved_dict 保存的训练结果